Projektfeladat BMEGEGTNMPR 21/22/1
==================================

Problem definition
------------------

Fit models that predict the predicted time to failure (PTTF) based on the measured degradation metric. The figure below plots the train set of the `~data.Degradation.dataset.DegradationPathDataset` as used in most of the model fittings developed.

.. image:: figures/degradation_trainset.png
   :width: 500
   :alt: Train set

Fitted models
-------------

linear regressor
^^^^^^^^^^^^^^^^

A linear regressor was fitted, mostly to give a baseline mean absolute error for further model fittings.

For low number of input features (<13), the model that predicted using the current degradation measurement and the two before performed the best, with a MAE of 110.0 measurement steps.

.. 
   image:: figures/linreg_2.png
   :width: 500
   :alt: linear regression over 2+1 features

For high number of input features, the models improved as the number of features increased. The model that used current and the previous 100 measurements had a MAE of 109.0 measurement steps, while the one that uses current and the 200 previous had a MAE of 108.4 measurement steps.

..
   image:: figures/linreg_200.png
   :width: 500
   :alt: linear regression over 200+1 features

multi-layer perceptron
^^^^^^^^^^^^^^^^^^^^^^

Varied were generally the depth of, the number of neurons in, and the number of input features fed to the networks.

The best performing network used the current degradation sample and the two before, had hidden neurons as (3, 4, 2) and a MAE of 104.0 measurement steps.

fully connected cascaded neural network
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Varied was the number of neurons in the network and the number of input features.

The best performing network had 9 hidden neurons on top of its 1 output, used the current and the 6 previous measurements of degradation and had a MAE of 104.5 measurement steps.

adaptive neuro-fuzzy inference system
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Varied were the number of rules in the rule-base and the number of input features.

The best performing ANFIS had 4 rules, used 8 previous measurements of degradation on top of the current one and had a MAE of 105.3 measurement steps.

Repository content
------------------

.. autosummary::
   :toctree: autosummary
   :template: module.rst
   :recursive:

   data
   model
   util
   train_degrad_linreg
   train_degrad_mlpreg
   train_degrad_fcc
   train_degrad_anfis
   hooks
   train_parityN_fcc
   train_parityN_anfis
   train_FashionMNIST_fcc
   notebooks
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
