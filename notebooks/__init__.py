"""Jupyter notebooks relevant to the project.
"""
import pathlib

THIS_MODULE = str(pathlib.Path(__file__).resolve().parent)
"""`pathlib.Path` to the module root."""