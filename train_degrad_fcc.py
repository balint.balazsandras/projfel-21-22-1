"""Train a fully connected cascade (FCC) neural network (NN) to predict degradation.
"""
import sys
from pathlib import Path
import time
import random
import torch
from torch import nn
from torch.utils.data import DataLoader
from torch.utils.data import random_split
import mlflow
from urllib.parse import urlparse
import warnings

from model.fcc import FCCRegressor
from model.fcc import ACTIVATIONS
from model.fcc import DEFAULT_DEVICE
from data.Degradation.dataset import DegradationPathDataset
from util.pytorch_training_tools import train_loop
from util.pytorch_training_tools import test_loop
from util.early_stopping.pytorchtools import EarlyStopping

SEED = 42
"""Default random seed to use for the experiments."""

BATCH_SIZE = 3
"""Default number of samples in a batch that the model 'sees' before updating its weights."""

EPOCHS = sys.maxsize * 2 + 1
"""Default number or epochs to train for.

The value is the interpreter's maximal word length, i.e., the biggest value that can be stored in an unsigned variable.
That is, infinity in practice, as the training should be stopped via early stopping.
"""

HIDDEN_LAYERS = 3
"""Default number of hidden layers."""

ACTIVATION = "relu"
"""Default non-linear activation after the hidden layers.

It was apparently more prudent to go for one that is available both in scikit-learn and PyTorch.
Also, scaling to avoid saturation is not necessarily at the top of one's list when implementing things. 
"""

DELTA = 1e-01
"""Default loss-change threshold over `PATIENCE` epochs for early stopping training."""

PATIENCE = 10
"""Default number of epochs to wait for the loss-drop to exceed `DELTA`."""

NDIFFS = 2
"""Default number of previous degradation values to use as input."""


def train_degrad_fcc(
    ndiffs: int = NDIFFS,
    batch_size: int = BATCH_SIZE,
    epochs: int = EPOCHS,
    hidden_layers: int = HIDDEN_LAYERS,
    activation: str = ACTIVATION,
    want_cuda: bool = False,
    seed: int = SEED,
    delta: float = DELTA,
    patience: float = PATIENCE,
    batch_norm: bool = False
) -> None:
    """Train a fully connected cascade (FCC) neural network (NN) to predict degradation.

    Parameters
    ----------
    ndiffs : int, optional
        Number of previous degradation values to use as input, by default `NDIFFS`
    batch_size : int, optional
        Number of samples shown to the model between weight updates, by default `BATCH_SIZE`
    epochs : int, optional
        Number of full iterations over the training set to train for, by default `EPOCHS`
    hidden_layers : int, optional
        Number of FCC hidden layers, i.e., number of neurons-1, by default `HIDDEN_LAYERS`
    activation : str, optional
        Non-linear activation between hidden layers, see `~model.fcc.ACTIVATIONS`, by default `ACTIVATION`
    want_cuda : bool, optional
        Whether using the GPU is preferred, by default `False`
    seed : int, optional
        Experiment random seed, by default `SEED`
    delta : float, optional
        Threshold for loss-drop over `patience` epochs to early-stop the training, by default `DELTA`
    patience : float, optional
        Number of epoch to wait for the loss-drop to exceed `delta`, by default `PATIENCE`
    batch_norm : bool, optional
        Whether to use batch normalization on the neuron outputs, by default `False`
    
    Returns
    -------
    tuple
        Tuple of (train set, train set, best model)
    """
    _ = mlflow.set_experiment(experiment_name="Degradation")
    
    torch.manual_seed(seed)  # seeding PyTorch
    random.seed(seed)  # seeding Python, just in case
    device = DEFAULT_DEVICE if want_cuda else "cpu"
    batch_norm = batch_norm and batch_size > 1  # a sanity-check -- batch norm. does not really like batch size 1
    warnings.warn("Batch normalization is ignored as batch size is 1.", UserWarning)

    with mlflow.start_run():
        # Get Dataset
        # -----------
        dataset = DegradationPathDataset(ndiffs=ndiffs)
        train_length = int(0.8 * len(dataset))
        train_set, test_set = random_split(
            dataset=dataset,
            lengths=[train_length, len(dataset) - train_length],
            generator=torch.Generator().manual_seed(2 * seed)
        )
        valid_length = int(0.2 * len(train_set))
        valid_train_set, valid_test_set = random_split(
            dataset=train_set,
            lengths=[len(train_set) - valid_length, valid_length],
            generator=torch.Generator().manual_seed(3 * seed)
        )

        # Init Dataloader
        # ---------------
        train_dataloader = DataLoader(
            valid_train_set,
            batch_size=batch_size,
            pin_memory=want_cuda,
            shuffle=True,
            drop_last=True,
            generator=torch.Generator().manual_seed(4 * seed)
        )
        test_dataloader = DataLoader(
            valid_test_set,
            batch_size=batch_size,
            pin_memory=want_cuda,
            shuffle=True,
            drop_last=True,
            generator=torch.Generator().manual_seed(5 * seed)
        )

        # Init model
        # ----------
        model = FCCRegressor(
            in_features=ndiffs + 1,
            hidden_layers=hidden_layers,
            out_features=1,
            bias=True,
            activation=activation,
            device=device,
            batch_normalization=batch_norm
        )

        # Init loss function
        # ------------------
        loss_fn = nn.L1Loss(reduction="mean")

        # Init optimizer
        # --------------
        optimizer = torch.optim.Adamax(model.parameters())
        
        # Init EarlyStopping
        # ------------------
        # to track losses as the model trains
        avg_train_losses = []
        avg_valid_losses = []
        # initialize the early_stopping object
        best_model = Path(__file__).resolve().parent / "logs" / "checkpoints" / f"checkpoint_{mlflow.active_run().info.run_id}.pt"
        best_model.parent.mkdir(exist_ok=True, parents=True)
        early_stopping = EarlyStopping(
            patience=patience,
            verbose=True,
            path=best_model,
            delta=delta
        )

        mlflow.log_param("model", "fcc")
        mlflow.log_param("ndiffs", ndiffs)
        mlflow.log_param("neurons", hidden_layers + 1)  # hidden neurons + the output neuron
        mlflow.log_param("hidden_layers", hidden_layers)
        mlflow.log_param("activation", activation)
        mlflow.log_param("batch_size", batch_size)
        mlflow.log_param("device", device)
        mlflow.log_param("seed", seed)
        mlflow.log_param("early_stop_threshold", delta)
        mlflow.log_param("early_stop_patience", patience)

        for t in range(epochs):
            print(f"Epoch {t+1:3d}", end=" | ")
            
            # train
            # -----
            avg_train_loss = train_loop(
                train_dataloader,
                model,
                loss_fn,
                optimizer,
                device=device
            )
            avg_train_losses.append(avg_train_loss)

            # validate
            # --------
            valid_loss, _ = test_loop(
                test_dataloader,
                model,
                loss_fn,
                device=device
            )
            avg_valid_losses.append(valid_loss)

            mlflow.log_metric(
                key="mae",
                value=valid_loss,
                step=t
            )
            mlflow.log_metric(
                key="elapsed_time",
                value=time.thread_time(),
                step=t
            )

            # early stop
            # ----------
            early_stopping(
                valid_loss,
                model
            )
            if early_stopping.early_stop:
                print("Early stopping")
                break

        mlflow.log_metric("early_stopped", int(early_stopping.early_stop))
        mlflow.log_metric("best_score", early_stopping.best_score)
        mlflow.log_metric("epochs", t)

        # load back best model
        model.load_state_dict(torch.load(best_model))
        model.eval()

        # log best model
        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme
        if tracking_url_type_store != "file":  # Model registry does not work with file store
            # Register the model
            mlflow.pytorch.log_model(model, "fcc_model", registered_model_name="FCCRegressor")
        else:
            mlflow.pytorch.log_model(model, "fcc_model")

    print("Done!")
    return train_set, test_set, model


if __name__=="__main__":
    # Application CLI
    # ---------------
    from argparse import ArgumentParser
    parser = ArgumentParser(
        description="Train a fully connected cascade (FCC) neural network (NN) to predict degradation."
    )
    parser.add_argument(
        "--ndiffs",
        type=int,
        default=NDIFFS,
        help=f"number of previous degradation values to use as input; default {NDIFFS}"
    )
    parser.add_argument(
        "--batch_size",
        type=int,
        default=BATCH_SIZE,
        help=f"number of samples shown to the model between weight updates; default {BATCH_SIZE}"
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=EPOCHS,
        help=f"number of epochs to train for; default {EPOCHS}"
    )
    parser.add_argument(
        "--hidden_layers",
        type=int,
        default=HIDDEN_LAYERS,
        help=f"number of model hidden layers, i.e., number of neurons - 1; default {HIDDEN_LAYERS}"
    )
    parser.add_argument(
        "--activation",
        type=str,
        choices=ACTIVATIONS,
        default=ACTIVATION,
        help=f"non-linear activation between hidden layers; default {ACTIVATION}"
    )
    parser.add_argument(
        "--want_cuda",
        action="store_true",
        help="whether using the GPU is preferred"
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=SEED,
        help=f"experiment random seed; default {SEED}"
    )
    parser.add_argument(
        "--delta",
        type=float,
        default=DELTA,
        help=f"threshold for loss-drop to stop the training over; default {DELTA}"
    )
    parser.add_argument(
        "--patience",
        type=float,
        default=PATIENCE,
        help=f"number of epoch to wait for the loss to drop by DELTA; default {PATIENCE}"
    )
    parser.add_argument(
        "--batch_norm",
        action="store_true",
        help="whether to use batch normalization on the neuron outputs"
    )

    args = parser.parse_args()

    train_degrad_fcc(**vars(args))
