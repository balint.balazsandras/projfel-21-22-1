#!/bin/bash
rm -r build
rm -r source/autosummary
cd ..
sphinx-build -b html docs/source docs/build
cd docs