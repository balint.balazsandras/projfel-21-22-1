"""Provide implementations of `~torch.utils.data.Dataset`.

By itself, a `~torch.utils.data.Dataset` is rarely to be used. It is usually sampled from using a `~torch.utils.data.DataLoader`.
"""
