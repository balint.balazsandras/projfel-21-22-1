"""Provides a PyTorch implementation for the adaptive neuro-fuzzy inference system (ANFIS).

API
---
"""
import torch
from torch import nn

IN_FEATURES = 1
"""Default number of input features."""

RULES = 2
"""Default number of rules in the rulebase."""


class SinglePremise(nn.Module):
    """Implements the premise of a single fuzzy Sugeno if-then rule.

    Parameters
    ----------
    in_features : int, optional
        Number of input features / variables, by default `IN_FEATURES`
    device : optional
        Pytorch device string, by default `None`
    dtype : optional
        Pytorch datatype, by default `None`

    Attributes
    ----------
    in_features : int
        Number of input features / variables
    mean : ~torch.Tensor
        `~torch.Tensor` of the Gaussian membership functions' mean values per input feature
    vari : ~torch.Tensor
        `~torch.Tensor` of the Gaussian membership functions' variance values per input feature
    """
    
    def __init__(self,
                 in_features: int = IN_FEATURES,
                 device=None,
                 dtype=None
    ) -> None:
        factory_kwargs = {'device': device, 'dtype': dtype}
        super(SinglePremise, self).__init__()

        self.in_features = in_features
        self.mean = nn.Parameter(torch.empty(self.in_features, **factory_kwargs))
        self.vari = nn.Parameter(torch.empty(self.in_features, **factory_kwargs))

        nn.init.uniform_(self.mean, a=-1.00, b=10.00)  # TODO: expose / clean-up / divine / something
        nn.init.uniform_(self.vari, a= 0.90, b= 1.10)  # TODO: expose / clean-up / divine / something
    
    def forward(self, x) -> torch.Tensor:
        mu = torch.exp(torch.pow((x - self.mean), 2) / (self.vari) * -1 / 2)
        return torch.prod(mu, dim=-1, keepdim=True)

    def extra_repr(self):
        return "IF " + " AND ".join([f"x{idx} is N({mean: .3f},{vari: .3f})" for idx, (mean, vari) in enumerate(zip(self.mean, self.vari))])


class ANFISPremise(nn.Module):
    """Implements the set of premises of a fuzzy Sugeno if-then rulebase.

    Parameters
    ----------
    in_features : int, optional
        Number of input features / variables, by default `IN_FEATURES`
    rules : int, optional
        Number of rules in the rulebase, by default `RULES`
    device : optional
        Pytorch device string, by default `None`
    dtype : optional
        Pytorch datatype, by default `None`

    Attributes
    ----------
    rules : int
        Number of rules in the rulebase
    ruleN : SinglePremise
        A `SinglePremise` in the rulesbase. Each of these can be accessed as attributes by replacing N with their index
    """

    def __init__(self,
                 in_features: int = IN_FEATURES,
                 rules: int = RULES,
                 device=None,
                 dtype=None
    ) -> None:
        factory_kwargs = {'device': device, 'dtype': dtype}
        super(ANFISPremise, self).__init__()

        self.rules = rules
        for rule in range(self.rules):
            self.__setattr__(f"rule{rule}", SinglePremise(in_features=in_features, **factory_kwargs))
    
    def forward(self, x) -> torch.Tensor:
        wlist = [self.__getattr__(f"rule{rule}")(x) for rule in range(self.rules)]
        return torch.cat(wlist, dim=-1)


class Normalize(nn.Module):
    """Implements a 'simple' normalization layer. 

    Parameters
    ----------
    dim : int, optional
        `~torch.Tensor` dimension to normalize along, by default -1
    
    Attributes
    ----------
    softmax : `~torch.nn.Softmax`
        Underlying `~torch.nn.Softmax` layer used to normalize
    """

    def __init__(self,
                 dim: int = -1
    ) -> None:
        super().__init__()
        self.softmax = nn.Softmax(dim=dim)
        
    def forward(self, x) -> torch.Tensor:
        return self.softmax(torch.log(x))


class ANFIS(nn.Module):
    """Implements the adaptive neural Sugeno-fuzzy inference (ANFIS) system.

    Parameters
    ----------
    in_features : int, optional
        Number of input features / variables, by default `IN_FEATURES`
    rules : int, optional
        Number of rules in the rulebase, by default `RULES`
    out_features : int, optional
        Number of output features / variables, by default 1
    dim : int, optional
        `~torch.Tensor` dimension to normalize along, by default -1
    device : optional
        Pytorch device string, by default `None`
    dtype : optional
        Pytorch datatype, by default `None`
    
    Attributes
    ----------
    premise : ANFISPremise
        Set of Sugeno-premises of the fuzzy if-then rulebase
    normalize : Normalize
        Normalization layer over the premises' firing values
    conclusion : ~torch.nn.Linear
        ~torch.nn.Linear conclusions of the first-order Sugeno if-then rules
    output : ~torch.nn.Linear
        Summing layer over the conclusions per the output features
    """
    
    def __init__(self,
                 in_features: int = IN_FEATURES,
                 rules: int = RULES,
                 out_features: int = 1,
                 dim: int = -1,
                 device=None,
                 dtype=None
    ) -> None:
        factory_kwargs = {'device': device, 'dtype': dtype}
        super().__init__()

        self.premise = ANFISPremise(rules=rules, in_features=in_features, **factory_kwargs)
        self.normalize = nn.Softmax(dim=dim)
        self.conclusion = nn.Linear(in_features=in_features, out_features=rules, **factory_kwargs)
        self.output = nn.Linear(in_features=rules, out_features=out_features, bias=False, **factory_kwargs)

        nn.init.uniform_(self.conclusion.weight)
        nn.init.uniform_(self.conclusion.bias)

        nn.init.ones_(self.output.weight)
        self.output.weight.requires_grad = False
    
    def forward(self, x) -> torch.Tensor:
        premise = self.normalize(self.premise(x))
        conclusion = self.conclusion(x)
        return self.output(premise * conclusion)
    
    def print_rulebase(self) -> None:
        """Print the rulebase to STDOUT / the console."""  # TODO: return the string?
        for idx in range(self.premise.rules):
            print(
                f"(R{idx})",
                self.premise.__getattr__(f"rule{idx}").extra_repr(),
                "THEN y=",
                " ".join(
                    [f"{coeff:+.3f} x{xid}" for xid, coeff in enumerate(self.conclusion.weight[idx, :])] + \
                        [f"{self.conclusion.bias[idx]:+.3f}"]
                )
            )
