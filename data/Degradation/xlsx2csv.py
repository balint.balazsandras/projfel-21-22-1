"""Function (and CLI app) to convert a degradation XLSX into a CSV."""
from pathlib import Path
import pandas as pd
from argparse import ArgumentParser

from .preprocess import DEFAULT_DATA


def xlsx2csv(path: str) -> None:
    """Convert the degradation XLSX into a CSV.

    Parameters
    ----------
    path : str
        Pathlike `str` to the degradation data XLSX file
    """
    data_file = Path(path)
    data = pd.read_excel(
        data_file,
        engine="openpyxl",
        index_col=None,
        sheet_name="Sheet1"
    )
    data.to_csv(data_file.parent / (data_file.stem + ".csv"), index=False)


if __name__=="__main__":
    # CLI
    # ---
    parser = ArgumentParser(description="Convert the degradation XLSX into a CSV.")
    parser.add_argument(
        "--data",
        type=str,
        default=DEFAULT_DATA,
        help=f"path to the degradation data XLSX file; default: {DEFAULT_DATA}")
    args = parser.parse_args()

    xlsx2csv(args.data)
