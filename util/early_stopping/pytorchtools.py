"""Early stopping for PyTorch

Early stopping is a form of regularization used to avoid overfitting on the training dataset. Early stopping keeps track of the validation loss, if the loss stops decreasing for several epochs in a row the training stops.
The `EarlyStopping` class is used to create an object to keep track of the validation loss while training a `PyTorch <https://pytorch.org/>`_ model. It will save a checkpoint of the model each time the validation loss decrease.
We set the `patience` argument in the `EarlyStopping` class to how many epochs we want to wait after the last time the validation loss improved before breaking the training loop.

Notes
-----
This module is originally from `this repository <https://github.com/Bjarten/early-stopping-pytorch>`_

The `EarlyStopping` class is inspired by the `ignite EarlyStopping class <https://github.com/pytorch/ignite/blob/master/ignite/handlers/early_stopping.py>`_

API
---
"""
import numpy as np
import math
import torch
from torch import nn


class EarlyStopping:
    """Early stops the training if validation loss doesn't improve after a given patience.
    
    Parameters
    ----------
    patience : int, optional
        How long to wait after last time validation loss improved, by default 7
    verbose : bool, optional
        If `True`, prints a message for each validation loss improvement, by default `False`
    delta : float, optional
        Minimum change in the monitored quantity to qualify as an improvement, by default 0.
    path : optional
        Path for the checkpoint to be saved to, see `torch.save`, by default 'checkpoint.pt'
    trace_func : optional
        By default `print`
    """
    
    def __init__(self, patience: int = 7, verbose: bool = False, delta: float = 0, path='checkpoint.pt', trace_func=print) -> None:
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf
        self.delta = delta
        self.path = path
        self.trace_func = trace_func
    
    def __call__(self, val_loss: float, model: nn.Module) -> None:
        """Implements the early stopping.

        Monitors the given metric and saves the model if there is improvement.

        Parameters
        ----------
        val_loss : float
            Value of the monitored loss metric
        model : ~nn.Module
            PyTorch model
        """
        score = -val_loss

        if self.best_score is None and not math.isnan(val_loss):
            self.best_score = score
            self.save_checkpoint(val_loss, model)
        elif score < (self.best_score + self.delta) or math.isnan(val_loss):
            self.counter += 1
            self.trace_func(f'EarlyStopping counter: {self.counter} out of {self.patience}')
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
            self.counter = 0

    def save_checkpoint(self, val_loss, model):
        '''Saves model when validation loss decreases.
        
        Parameters
        ----------
        val_loss : float
            Value of the monitored loss metric
        model : ~nn.Module
            PyTorch model
        '''
        if self.verbose:
            self.trace_func(f'Validation loss decreased ({self.val_loss_min:.6f} --> {val_loss:.6f}).  Saving model ...')
        torch.save(model.state_dict(), self.path)
        self.val_loss_min = val_loss
