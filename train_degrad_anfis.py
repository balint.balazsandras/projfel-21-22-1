"""Train an adaptive neuro-fuzzy inference system (ANFIS) to predict degradation.
"""
import sys
from pathlib import Path
import time
import random
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.utils.data import random_split
import mlflow
from urllib.parse import urlparse

from data.Degradation.dataset import DegradationPathDataset
from model.anfis import ANFIS
from train_parityN_fcc import train_loop
from train_parityN_fcc import test_loop
from util.early_stopping.pytorchtools import EarlyStopping

DEFAULT_DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
"""Pytorch device string that is 'cuda' if available."""

IN_FEATURES = 1
"""Default number of input features."""

RULES = 2
"""Default number of rules in the rulebase."""

BATCH_SIZE = 3
"""Default number of samples in a batch that the model 'sees' before updating its weights."""

EPOCHS = sys.maxsize * 2 + 1  # max. value that an unsigned variable can have
"""Default number or epochs to train for.

The value is the interpreter's maximal word length, i.e., the biggest value that can be stored in an unsigned variable.
That is, infinity in practice, as the training should be stopped via early stopping.
"""

SEED = 42
"""Default seed to use in the experiment"""

DELTA = 1e-01
"""Default loss-change threshold over `PATIENCE` epochs for early stopping training."""

PATIENCE = 10
"""Default number of epochs to wait for the loss-drop to exceed `DELTA`."""

NDIFFS = 2
"""Default number of previous degradation values to use as input."""


def train_degrad_anfis(
    ndiffs: int = NDIFFS,
    batch_size: int = BATCH_SIZE,
    epochs: int = EPOCHS,
    rules: int = RULES,
    want_cuda: bool = False,
    seed: int = SEED,
    delta: float = DELTA,
    patience: float = PATIENCE
) -> None:
    """Train an adaptive neuro-fuzzy inference system (ANFIS) to predict degradation.

    Parameters
    ----------
    ndiffs : int, optional
        Number of previous degradation values to use as input, by default `NDIFFS`
    batch_size : int, optional
        Number of samples shown to the model between weight updates, by default `BATCH_SIZE`
    epochs : int, optional
        Number of full iterations over the training set to train for, by default `EPOCHS`
    rules : int, optional
        Number of rules to use in the rulebase, by default `RULES`
    want_cuda : bool, optional
        Whether using the GPU is preferred, by default `False`
    seed : int, optional
        Experiment random seed, by default `SEED`
    delta : float, optional
        Threshold for loss-drop over `patience` epochs to early-stop the training, by default `DELTA`
    patience : float, optional
        Number of epoch to wait for the loss-drop to exceed `delta`, by default `PATIENCE`

    Returns
    -------
    tuple
        Tuple of (train set, train set, best model)
    """
    _ = mlflow.set_experiment(experiment_name="Degradation")
    
    torch.manual_seed(seed)  # seeding PyTorch
    random.seed(seed)  # seeding Python, just in case
    device = DEFAULT_DEVICE if want_cuda else "cpu"

    with mlflow.start_run():
        # Get Dataset
        # -----------
        dataset = DegradationPathDataset(ndiffs=ndiffs)

        train_length = int(0.8 * len(dataset))
        train_set, test_set = random_split(dataset=dataset, lengths=[train_length, len(dataset) - train_length], generator=torch.Generator().manual_seed(2 * seed))
        
        valid_length = int(0.2 * len(train_set))
        valid_train_set, valid_test_set = random_split(
            dataset=train_set,
            lengths=[len(train_set) - valid_length, valid_length],
            generator=torch.Generator().manual_seed(3 * seed)
        )

        # Init Dataloader
        # ---------------
        train_dataloader = DataLoader(
            valid_train_set,
            batch_size=batch_size,
            pin_memory=want_cuda,
            shuffle=True,
            drop_last=False,
            generator=torch.Generator().manual_seed(4 * seed)
        )
        test_dataloader = DataLoader(
            valid_test_set,
            batch_size=batch_size,
            pin_memory=want_cuda,
            shuffle=True,
            drop_last=False,
            generator=torch.Generator().manual_seed(5 * seed)
        )

        # Init model
        # ----------
        model = ANFIS(
            in_features=ndiffs + 1,
            rules=rules,
            out_features=1,
            device=device
        )

        # Init loss function
        # ------------------
        loss_fn = nn.L1Loss(reduction="mean")

        # Init optimizer
        # --------------
        optimizer = torch.optim.Adamax(model.parameters())

        # Init EarlyStopping
        # ------------------
        # to track losses as the model trains
        avg_train_losses = []
        avg_valid_losses = []
        # initialize the early_stopping object
        best_model = Path(__file__).resolve().parent / "logs" / "checkpoints" / f"checkpoint_{mlflow.active_run().info.run_id}.pt"
        best_model.parent.mkdir(exist_ok=True, parents=True)
        early_stopping = EarlyStopping(
            patience=patience,
            verbose=True,
            path=best_model,
            delta=delta
        )

        mlflow.log_param("model", "anfis")
        mlflow.log_param("ndiffs", ndiffs)
        mlflow.log_param("rules", rules)
        mlflow.log_param("batch_size", batch_size)
        mlflow.log_param("device", device)
        mlflow.log_param("seed", seed)
        mlflow.log_param("early_stop_threshold", delta)
        mlflow.log_param("early_stop_patience", patience)

        for t in range(epochs):
            print(f"Epoch {t+1:3d}", end=" | ")

            # train
            # -----
            avg_train_loss = train_loop(
                train_dataloader,
                model,
                loss_fn,
                optimizer,
                device
            )
            avg_train_losses.append(avg_train_loss)

            # validate
            # --------
            valid_loss, _ = test_loop(
                test_dataloader,
                model,
                loss_fn,
                device
            )
            avg_valid_losses.append(valid_loss)

            mlflow.log_metric(
                key="mae",
                value=valid_loss,
                step=t
            )
            mlflow.log_metric(
                key="elapsed_time",
                value=time.thread_time(),
                step=t
            )

            # early stop
            # ----------
            early_stopping(
                valid_loss,
                model
            )
            if early_stopping.early_stop:
                print("Early stopping")
                break
            
        mlflow.log_metric("early_stopped", int(early_stopping.early_stop))
        mlflow.log_metric("best_score", early_stopping.best_score)
        mlflow.log_metric("epochs", t)

        # load back best model
        model.load_state_dict(torch.load(best_model))
        model.eval()

        # log best model
        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme
        if tracking_url_type_store != "file":  # Model registry does not work with file store
            # Register the model
            mlflow.pytorch.log_model(model, "anfis_model", registered_model_name="ANFIS")
        else:
            mlflow.pytorch.log_model(model, "anfis_model")

        # print("--- RULEBASE ---")
        # model.print_rulebase()
        return train_set, test_set, model


if __name__=="__main__":
    # Application CLI
    # ---------------
    from argparse import ArgumentParser
    parser = ArgumentParser(
        description="Train a fully connected cascade (FCC) neural network (NN) to solve a parity-N problem."
    )
    parser.add_argument(
        "--ndiffs",
        type=int,
        default=NDIFFS,
        help=f"number of previous degradation values to use as input; default {NDIFFS}"
    )
    parser.add_argument(
        "--batch_size",
        type=int,
        default=BATCH_SIZE,
        help=f"number of samples shown to the model between weight updates; default {BATCH_SIZE}"
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=EPOCHS,
        help=f"number of epochs to train for; default {EPOCHS}"
    )
    parser.add_argument(
        "--rules",
        type=int,
        default=RULES,
        help=f"number of rules in the rulesbase; default {RULES}"
    )
    parser.add_argument(
        "--want_cuda",
        action="store_true",
        help="whether using the GPU is preferred"
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=SEED,
        help=f"experiment random seed; default {SEED}"
    )
    parser.add_argument(
        "--delta",
        type=float,
        default=DELTA,
        help=f"threshold for loss-drop to stop the training over; default {DELTA}"
    )
    parser.add_argument(
        "--patience",
        type=float,
        default=PATIENCE,
        help=f"threshold for loss-drop to stop the training over; default {PATIENCE}"
    )

    args = parser.parse_args()

    train_degrad_anfis(**vars(args))
