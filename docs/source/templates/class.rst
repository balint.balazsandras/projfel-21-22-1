{{ name | escape | underline}}

.. currentmodule:: {{ module }}

.. autoclass:: {{ objname }}
   :members:
   :private-members:
   :special-members:
   :exclude-members: __init__
   :no-undoc-members:
