"""Train a fully connected cascade (FCC) neural network (NN) on the fashion MNIST dataset.
"""
import torch
from torch import nn
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
from argparse import ArgumentParser
from pathlib import Path
import mlflow

from model.fcc import FCCRegressor
from model.fcc import FCCClassifier
from util.pytorch_training_tools import train_loop
from util.pytorch_training_tools import test_loop
from util.early_stopping.pytorchtools import EarlyStopping


DEFAULT_LEARNING_RATE = 1e-3
"""Default learning rate for the Stochastic Gradient Descent."""

DEFAULT_BATCH_SIZE = 64
"""Default number of elements to show to the network between weight updates."""

DEFAULT_EPOCHS = 10
"""Default number of complete scans over the train dataset to train for."""

DEFAULT_MODEL = "fccreg"
"""Default model to train, i.e., `~model.fcc.FCCRegressor`."""


def get_datasets() -> tuple:
    """Return the FashionMNIST train and test datasets.

    Returns
    -------
    tuple
        Tuple of (train dataset, test dateset)
    """
    training_data = datasets.FashionMNIST(
        root="data",
        train=True,
        download=True,
        transform=ToTensor()
    )

    test_data = datasets.FashionMNIST(
        root="data",
        train=False,
        download=True,
        transform=ToTensor()
    )
    return training_data, test_data


def get_dataloaders(
    training_data: Dataset,
    test_data: Dataset,
    batch_size: int = DEFAULT_BATCH_SIZE
) -> tuple:
    """Return the sampling strategies for the train and test datasets.

    Parameters
    ----------
    training_data : ~torch.utils.data.Dataset
        Train dataset.
    test_data : ~torch.utils.data.Dataset
        Test dataset
    batch_size : int
        Number of samples to show to the model before updating its weights, by default `DEFAULT_BATCH_SIZE`

    Returns
    -------
    tuple
        Tuple of (train dataloader, test dataloader)
    """
    train_dataloader = DataLoader(training_data, batch_size=batch_size, pin_memory=True)
    test_dataloader = DataLoader(test_data, batch_size=batch_size, pin_memory=True)
    return train_dataloader, test_dataloader


class NeuralNetwork(nn.Module):
    """A simple (512, 512, 10) multi-layer perceptron.
    """

    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.flatten = nn.Flatten()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(28*28, 512),  # sourcery skip: square-identity
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 10)
        )

    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_relu_stack(x)  # sourcery skip: inline-immediately-returned-variable
        return logits


def get_model(model: str = "fccreg") -> None:
    """Return the model to be trained.

    Parameters
    ----------
    model : str, optional, {"fccreg", "fcccla"}
        Name of the model, by default "fccreg"

    Returns
    -------
    ~torch.nn.Module
        PyTorch model to train; if model was not given, an instance of `NeuralNetwork` is returned
    """
    # sourcery skip: lift-return-into-if, square-identity
    if model == "fccreg":
        model = nn.Sequential(nn.Flatten(), FCCRegressor(in_features=28*28, out_features=10, hidden_layers=2))
    elif model == "fcccla":
        model = nn.Sequential(nn.Flatten(), FCCClassifier(in_features=28*28, out_features=10, hidden_layers=2))
    else:
        model = NeuralNetwork()
    return model


def main(
    learning_rate: float = DEFAULT_LEARNING_RATE,
    batch_size: int = DEFAULT_BATCH_SIZE,
    epochs: int = DEFAULT_EPOCHS,
    model: str = DEFAULT_MODEL
) -> None:
    """Train a PyTorch model over the FashionMNIST dataset.

    Parameters
    ----------
    learning_rate : float, optional
        Learning rate of the optimizer, see `~torch.optim.SGD`, by default `DEFAULT_LEARNING_RATE`
    batch_size : int, optional
        Number of samples to show to the model before updating its weights, by default `DEFAULT_BATCH_SIZE`
    epochs : int, optional
        Number of complete scans over the train dataset to train for, by default `DEFAULT_EPOCHS`
    model : str, optional
        Name of the PyTorch model to train, see `get_model`, by default `DEFAULT_MODEL`
    """
    training_data, test_data = get_datasets()
    train_dataloader, test_dataloader = get_dataloaders(training_data, test_data, batch_size)
    model = get_model(model=model)

    # Initialize the loss function
    loss_fn = nn.CrossEntropyLoss()

    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

    # to track losses as the model trains
    avg_train_losses = []
    avg_valid_losses = []
    # initialize the early_stopping object
    best_model = Path(__file__).resolve().parent / "logs" / "checkpoints" / f"checkpoint_{mlflow.active_run().info.run_id}.pt"
    best_model.parent.mkdir(exist_ok=True, parents=True)
    early_stopping = EarlyStopping(
        patience=10,  # TODO: expose as hyperparameter?
        verbose=True,
        path=best_model
    )

    for t in range(epochs):
        print(f"Epoch {t+1:3d}", end=" | ")

        avg_train_loss = train_loop(
            train_dataloader,
            model,
            loss_fn,
            optimizer,
            "cpu"
        )
        avg_train_losses.append(avg_train_loss)

        avg_valid_loss = test_loop(
            test_dataloader,
            model,
            loss_fn,
            "cpu"
        )
        avg_valid_losses.append(avg_valid_loss)

        early_stopping(avg_valid_loss, model)
        if early_stopping.early_stop:
            print("Early stopping")
            break

    mlflow.log_param("model", model)
    mlflow.log_param("learning_rate", learning_rate)
    mlflow.log_param("batch_size", batch_size)
    mlflow.log_param("epochs", t)
    mlflow.log_param("device", "cpu")

    mlflow.log_metric("early_stopped", int(early_stopping.early_stop))
    mlflow.log_metric("best_score", early_stopping.best_score)
    
    print(model)
    print("Done!")


if __name__=="__main__":
    parser = ArgumentParser(
        description="Train a fully connected cascade (FCC) neural network (NN) on the fashion MNIST dataset."
    )
    parser.add_argument(
        "--learning-rate",
        type=float,
        default=DEFAULT_LEARNING_RATE,
        help=f"learning rate for the Stochastic Gradient Descent; default={DEFAULT_LEARNING_RATE}"
        )
    parser.add_argument(
        "--batch-size",
        type=int,
        default=DEFAULT_BATCH_SIZE,
        help=f"number of elements to show to the model between weight upgrades (epochs); default={DEFAULT_BATCH_SIZE}"
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=DEFAULT_EPOCHS,
        help=f"number of epochs (weight upgrades) to train for; default={DEFAULT_EPOCHS}"
    )
    parser.add_argument(
        "--model",
        type=str,
        default=DEFAULT_MODEL,
        help=f"the model to use, can be \"fccreg\" for FCC regressor, \"fcccla\" for FCC classifier, or anything else for the baseline NN; default={DEFAULT_MODEL}"
    )
    args = parser.parse_args()
    main(**vars(args))
