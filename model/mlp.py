"""Provides a PyTorch implementation for a multi-layer perceptron (MLP) topology both for regression and classification.

API
---
"""
import torch
from torch import nn
from torch import Tensor

from .fcc import ACTIVATIONS
from .fcc import INITIALIZATIONS

DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
"""Default device on which to set `~torch.Tensors` up. 'cuda' if it is available, 'cpu' otherwise."""


class MLPRegressor(nn.Module):
    """Implements a multi-layer perceptron (MLP) regressor.
    
    Parameters
    ----------
    in_features : int
        Number of (flattened) input features
    hidden_layer_sizes : tuple, optional
        `tuple` of `int` with the number of neurons in the hidden layers, by default (100,)
    out_features : int, optional
        Number of output features, by default 1
    bias : bool, optional
        Whether to use bias terms in the `~torch.nn.Linear` layers, by default `True`
    activation : str, optional
        Non-linear activation, by default "relu"
    batch_normalization : bool, optional
        Whether to apply `~torch.nn.BatchNorm1d` after the non-linear activation, by default `False`
    eps : float, optional
        A value added to the denominator for numerical stability, by default 1e-05, see `~torch.nn.BatchNorm1d`
    momentum : float, optional
        Value used for the running mean variance computation, by default 0.1, see `~torch.nn.BatchNorm1d`
    affine : bool, optional
        Whether the `~torch.nn.BatchNorm1d` layer is trainable, by default `True`
    track_running_stats : bool, optional
        Whether to use running or (batch) statistics , by default `True`, see `~torch.nn.BatchNorm1d`
    device : optional
        Device on which to set `~torch.Tensors` up, by default `None`, see `~torch.nn.Linear`
    dtype : optional
        Datatype of parameters, by default `None`, see `~torch.nn.Linear`

    Attributes
    ----------
    layer_stack : ~torch.nn.Sequential
        PyTorch layer stack, i.e., the actual model
    """
        
    def __init__(
        self,
        in_features: int,
        hidden_layer_sizes: tuple = (100,),
        out_features: int = 1,
        bias: bool = True,
        activation: str = "relu",
        batch_normalization: bool = False,
        eps: float = 1e-05,
        momentum: float = 0.1,
        affine: bool = True,
        track_running_stats: bool = True,
        device=None,
        dtype=None
    ) -> None:
        factory_kwargs = {"device": device, "dtype": dtype}
        super(MLPRegressor, self).__init__()
        sizes = [in_features, *hidden_layer_sizes]
        layers = []
        for in_feats, out_feats in zip(sizes, sizes[1:]):
            layers.append(nn.Linear(in_features=in_feats, out_features=out_feats, bias=bias, **factory_kwargs))
            INITIALIZATIONS[activation](layers[-1].weight)
            # bias and INITIALIZATIONS[activation](layers[-1].bias)
            layers.append(ACTIVATIONS[activation]())
            batch_normalization and layers.append(nn.BatchNorm1d(
                num_features=out_feats,
                eps=eps,
                momentum=momentum,
                affine=affine,
                track_running_stats=track_running_stats,  # TODO: implement a more educated training?
                **factory_kwargs
            ))
        layers.append(nn.Linear(
            in_features=hidden_layer_sizes[-1],
            out_features=out_features,
            bias=bias,
            **factory_kwargs
        ))
        INITIALIZATIONS[activation](layers[-1].weight)
        # bias and INITIALIZATIONS[activation](layers[-1].bias)    
        self.layer_stack = nn.Sequential(*layers)
    
    def forward(self, x: Tensor) -> Tensor:
        return self.layer_stack(x)


class MLPClassifier(MLPRegressor):
    """Implements a multi-layer perceptron (MLP) classifier.
    
    Parameters
    ----------
    output_activation : str, optional
        Non-linear activation to apply for the output layer, by default "softmax", see `ACTIVATIONS`
    kwargs : dict, optional
        See `MLPRegressor`

    Attributes
    ----------
    layer_stack : ~torch.nn.Sequential
        PyTorch layer stack, i.e., the actual model

    Notes
    -----
    Practically, the classifier is an `MLPRegressor` with a `~torch.nn.SoftMax` activation attached to it.
    """
    
    def __init__(self,
                 output_activation: str = "softmax",
                 **kwargs
    ) -> None:
        super(MLPClassifier, self).__init__(**kwargs)
        self.output_activation = ACTIVATIONS[output_activation]
    
    def forward(self, x: Tensor) -> Tensor:
        regressor_output = super(MLPClassifier, self).forward(x)
        return self.output_activation(regressor_output)
