"""Train a linear model to predict degradation.

API
---
"""
from sklearn.linear_model import ElasticNet
from sklearn.model_selection import cross_validate
import mlflow
import mlflow.sklearn
from urllib.parse import urlparse

from data.Degradation.preprocess import preprocess
from data.Degradation.preprocess import inverse_transform_scores
from train_degrad_anfis import NDIFFS

METRICS = ("neg_mean_absolute_error", "max_error")
"""Default metrics over which to cross-validate the models."""

NDIFFS = 2
"""Default number of previous degradation values to use as input."""

TEST_SIZE = 0.2
"""Default ratio of test data wrt. the whole dataset."""

SEED = 42
"""Default random seed to use for the experiments."""

CV = 7
"""Default number of folds in cross-validation."""

TRANSFORM = False
"""Default flag whether to fit an `~sklearn.preprocessing.RobustScaler` to the train dataset."""


def cross_val_model(
    model,
    data,
    label,
    cv: int = CV,
    transformer=None
) -> None:
    """Cross-validate a scikit-learn model over a train dataset.

    Parameters
    ----------
    model
        Scikit-learn model to cross-validate
    data
        Train dataset
    label
        Target labels for the train data
    cv : int, optional
        Number of folds in cross-validation, by default `CV`
    transformer : optional
        Scaler, if the train data are scaled, by default `None`

    Returns
    -------
    dict
        Cross-validation scores dictionary

    Notes
    -----
    Properly processing scaled data is a feature incomplete.
    """
    scores = cross_validate(model, data, label, cv=cv,
                            scoring=METRICS)
    if transformer is not None:
        for metric in METRICS:
            scores["test_" + metric] = inverse_transform_scores(scores["test_" + metric], "PTTF", label, transformer)

    return scores


def display_scores(scores) -> None:
    """Display the cross-validation scores returned by `cross_val_model`.

    Parameters
    ----------
    scores : dict
        Cross-validation scores dictionary
    """
    for metric in METRICS:
        print("\n", metric)
        print("-" * len(metric))
        print("  Scores:", scores["test_" + metric])
        print("  Mean:", scores["test_" + metric].mean())
        print("  Std: ", scores["test_" + metric].std())


def train_degrad_linreg(
    ndiffs: int = NDIFFS,
    test_size: float = TEST_SIZE,
    seed: int = SEED,
    cv: int = CV,
    transform: bool = TRANSFORM
) -> tuple:
    """Train a linear regressor to predict degradation.

    Parameters
    ----------
    ndiffs : int, optional
        Number of previous degradation values to use as input, by default `NDIFFS`
    test_size : float, optional
        Ratio of test data wrt. the whole dataset, by default `TEST_SIZE`
    seed : int, optional
        Experiment random seed, by default `SEED`
    cv : int, optional
        Number of folds in cross-validation, by default `CV`
    transform : bool, optional
        Whether to fit an `~sklearn.preprocessing.RobustScaler` to the train dataset, by default `TRANSFORM`

    Returns
    -------
    tuple
        Tuple of (train set, test set, unfitted model, cross validation scores)
    """
    train_set, test_set, transformer = preprocess(ndiffs=ndiffs, test_size=test_size, seed=seed, transform=transform)

    data = train_set.drop("PTTF", axis=1)
    label = train_set["PTTF"].copy()


    _ = mlflow.set_experiment(experiment_name="Degradation")
    with mlflow.start_run():
        lin_reg = ElasticNet(
            max_iter=10000,
            random_state=seed
        )

        mlflow.log_param("model", "linreg")
        mlflow.log_param("ndiffs", ndiffs)
        mlflow.log_param("neurons", 1)
        mlflow.log_param("scale", transform)
        mlflow.log_param("test_size", test_size)
        mlflow.log_param("cv", cv)
        mlflow.log_param("seed", seed)

        scores = cross_val_model(lin_reg, data, label, cv=cv, transformer=transformer if transform else None)
        display_scores(scores)

        mlflow.log_metric("best_score", scores["test_neg_mean_absolute_error"].mean())
        mlflow.log_metric("mean_MAE", -scores["test_neg_mean_absolute_error"].mean())
        mlflow.log_metric("std_MAE", scores["test_neg_mean_absolute_error"].std())
        mlflow.log_metric("mean_max_err", -scores["test_max_error"].mean())
        mlflow.log_metric("std_max_err", scores["test_max_error"].std())
        mlflow.log_metric("mean_fit_time", scores["fit_time"].mean())
        mlflow.log_metric("std_fit_time", scores["fit_time"].std())

        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme
        if tracking_url_type_store != "file":  # model registry does not work with file store
            mlflow.sklearn.log_model(lin_reg, "model", registered_model_name="LinearRegression")
        else:
            mlflow.sklearn.log_model(lin_reg, "model")

    return train_set, test_set, lin_reg, scores


if __name__=="__main__":
    # Application CLI
    # ---------------
    from argparse import ArgumentParser

    parser = ArgumentParser(description="Train a linear regressor to predict degradation.")
    parser.add_argument(
        "--ndiffs",
        type=int,
        default=NDIFFS,
        help=f"number of previous degradation values to use as input; default {NDIFFS}"
    )
    parser.add_argument(
        "--test_size",
        type=float,
        default=TEST_SIZE,
        help=f"ratio of test data wrt. the whole dataset; default: {TEST_SIZE}"
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=SEED,
        help=f"experiment random seed; default {SEED}"
    )
    parser.add_argument(
        "--cv",
        type=int,
        default=CV,
        help=f"number of folds in cross-validation; default: {CV}"
    )
    parser.add_argument(
        "--transform",
        action="store_true",
        help="whether to fit a scaler to the dataset"
    )
    args = parser.parse_args()

    train_degrad_linreg(**vars(args))
