"""Utility functions that wrap training and validation in an epoch during a PyTorch model training.

API
---
"""
import torch
from torch import nn
from torch.utils.data import DataLoader
from typing import Tuple

from .rolling import rolling_avg


def train_loop(
    dataloader: DataLoader,
    model: nn.Module,
    loss_fn,
    optimizer,
    device,
    verbose: bool = True
) -> list:
    """Train a PyTorch model for an epoch.

    Parameters
    ----------
    dataloader : ~torch.utils.data.DataLoader
        PyTorch `~torch.utils.data.DataLoader` that implements the sampling of the training set
    model : ~torch.nn.Module
        Pytorch model to train
    loss_fn
        PyTorch loss function, see `torch.nn`
    optimizer
        PyTorch optimizer, see `torch.optim`
    device
        PyTorch device string
    verbose: bool, optional
        Verbosity level; if `True` the function also prints the average train loss, by default `True`

    Returns
    -------
    avg_train_loss : list
        Average training loss over the epoch
    """
    avg_train_loss = 0
    for batch, (X, y) in enumerate(dataloader):
        # Compute prediction and loss
        pred = model(X.to(device))
        loss = loss_fn(pred, y.to(device))

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # Log losses
        avg_train_loss, _ = rolling_avg(avg_train_loss, batch, loss.item())
    
    if verbose:
        print(f"train avg. loss: {avg_train_loss:>8f}", end=" | ")

    return avg_train_loss


def test_loop(
    dataloader: DataLoader,
    model: nn.Module,
    loss_fn,
    device,
    verbose: bool = True
) -> Tuple[float, float]:
    """Validate a PyTorch model over the test set.

    Parameters
    ----------
    dataloader : DataLoader
        PyTorch `~torch.utils.data.DataLoader` that implements the sampling of the test set
    model : nn.Module
        PyTorch model to validate
    loss_fn
        PyTorch loss function, see `torch.nn`
    device
        PyTorch device string
    verbose: bool, optional
        Verbosity level; if `True` the function also prints the average test loss, by default `True`

    Returns
    -------
    Tuple[float, float]
        Tuple of average test loss over the batches and the accuracy of the model

    Notes
    -----
    The accuracy returned is not a general, but a custom metric for the `ParityN` problems. 
    """
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X.to(device))
            test_loss += loss_fn(pred, y.to(device)).item()

            # The line below is to calculate accuracy and may need to be adapted to the use-case
            correct += (torch.round(pred) == y.to(device)).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size

    if verbose:
        print(f"test avg. loss: {test_loss:>8f}", end=" | ")

    return test_loss, correct  # TODO: generalize / remove accuracy
