#!/usr/bin/env python
import sys
from pathlib import Path
from subprocess import run

THIS_FILE = Path(__file__).resolve().parent
"""`pathlib.Path` to this file.
"""


def post_merge_hook(argv: tuple) -> None:
    """Convert version controlled .py jupyter notebooks to .ipynb using jupytext.

    Parameters
    ----------
    argv : tuple
        System argument vector; for a post-merge hook, this is a single boolean indicating whether the merge was a squash merge.
    """
    is_squash = argv[1]
    print("This {} a squash merge. (is_squash: {})".format("is" if bool(is_squash) else "isn't", is_squash))

    notebooks = THIS_FILE.parent / "notebooks"
    for path in notebooks.glob("*.py"):
        path.stem != "__init__" and run(["jupytext", "--sync", str(path)])


if __name__=="__main__":
    post_merge_hook(sys.argv)
    