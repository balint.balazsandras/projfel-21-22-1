"""Provide a Party-N map-type PyTorch `~torch.utils.data.Dataset`."""
import torch
from torch.utils.data import Dataset
import numpy as np
from functools import lru_cache
from typing import Tuple


class ParityN(Dataset):
    """PyTorch map-type `~torch.utils.data.Dataset` for a parity-N problem.
    
    Parameters
    ----------
    N : int
        The N in the parity-N
    
    Attributes
    ----------
    N : int
        The N in the parity-N
    _length : int
        Attribute to store the length of the `~torch.utils.data.Dataset`.
    """

    def __init__(self, N: int) -> None:
        super().__init__()
        self.N = N
        self._length = 2**self.N
    
    def _unpack(self, d: np.ndarray, m: int) -> np.ndarray:
        """Unpacks an integer into an array of its binary representation with a given padding.

        Parameters
        ----------
        d : ~numpy.ndarray
            Integer to unpack as a 1x1 ~numpy.ndarray
        m : int
            Number of bits to pad to

        Returns
        -------
        ~numpy.ndarray
            Unpacked array of padded 1s and 0s
        
        Examples
        --------
        >>> import numpy as np
        >>> from data.Degradation.ParityN.dataset import ParityN
        >>> dataset = ParityN(1)
        >>> dataset._unpack(np.array([11]), 6)
        array([[1, 1, 0, 1, 0, 0]])

        Notes
        -----
        Source: https://stackoverflow.com/a/22227898
        """
        return (((d[:,None] & (1 << np.arange(m)))) > 0).astype(int)
    
    @lru_cache
    def __getitem__(self, index: int) -> Tuple[torch.Tensor, torch.Tensor]:
        """Get the sample with the given index.

        Parameters
        ----------
        index : int
            Index of the requested sample

        Returns
        -------
        ~typing.Tuple[~torch.Tensor, ~torch.Tensor]
            Pair of `~torch.Tensor`, (sample, label)
        """
        return \
            torch.Tensor(self._unpack(np.array([index]), m=self.N)), \
            torch.Tensor([[int(index % 2 != 0)]])  # TODO: change returned label for classification to [1, 0] or [0, 1]

    def __len__(self):
        return self._length
