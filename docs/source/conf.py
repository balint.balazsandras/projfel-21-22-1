import pathlib
import sys

sys.path.insert(
    0,
    str(pathlib.Path(__file__).resolve().parents[2])
)

print("PATH")
print("----")
for path in sys.path:
    print(" ", path)
print("----")

# Project information
# ===================
project = 'Projektfeladat BMEGEGTNMPR 21/22/1'
copyright = '2021, Bálint Balázs András'
author = 'Bálint, Balázs András'

# The short X.Y version
version = '0.0'

# The full version, including alpha/beta/rc tags
release = '0.0'

# General configuration
# =====================
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'numpydoc',
    'sphinx.ext.viewcode',
    'sphinx_rtd_theme'
]

templates_path = ['templates']

language = 'en'

exclude_patterns = [
    '_build',
    'Thumbs.db',
    '.DS_Store'
]

default_role = 'obj'

pygments_style = 'sphinx'

strip_signature_backslash = True

add_module_names = False

# Options for HTML output
# ===================
# html_theme = 'scipy'
# html_theme_path = [os.path.abspath('theme')]
# html_static_path = [os.path.abspath('theme/scipy/static')]

html_theme = "sphinx_rtd_theme"
html_theme_options = {
    # Misc. options
    'logo_only': False,
    'display_version': True,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': False,
    'vcs_pageview_mode': '',
    'style_nav_header_background': 'white',
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': -1,
    'includehidden': True,
    'titles_only': False
}

html_sidebars = {
   '**': [
       'globaltoc.html',
       'sourcelink.html',
       'searchbox.html'
    ]
}

# Extension configuration
# =======================

# autodoc & Co.
# -------------
autodoc_typehints = "none"
autosummary_generate = True

# intersphinx
# -----------
intersphinx_mapping = {  # TODO: complete references
    'python': ('https://docs.python.org/3', None),
    'torch': ('https://pytorch.org/docs/master/', None),
    'sklearn': ('http://scikit-learn.org/stable', None),
    'numpy': ('https://numpy.org/doc/stable/', None),
    'pandas': ('https://pandas.pydata.org/pandas-docs/dev/', None),
}

# numpydoc
# --------
import numpydoc

numpydoc_xref_param_type = True
numpydoc_xref_ignore = {
    'optional',
    'type_without_description',
    'BadException'
}
numpydoc_xref_aliases = {}
numpydoc_class_members_toctree = False
numpydoc_attributes_as_param_list = True
numpydoc_show_class_members = True
numpydoc_show_inherited_class_members = False

# todo
# ----
# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True
