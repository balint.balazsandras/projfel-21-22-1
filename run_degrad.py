import multiprocessing as mp
from itertools import product

from train_degrad_linreg import train_degrad_linreg
from train_degrad_anfis import train_degrad_anfis
from train_degrad_fcc import train_degrad_fcc
from train_degrad_mlpreg import train_degrad_mlp


def _train_linreg(argv):
    seed, cv, transform, ndiff = argv
    train_degrad_linreg(
        ndiffs=ndiff,
        test_size=0.2,
        seed=seed,
        cv=cv,
        transform=transform
    )


def _train_fccreg(argv):
    ndiff, batch_size, batch_norm, activation, want_cuda, seed, hidden_layer = argv
    train_degrad_fcc(
        ndiffs=ndiff,
        batch_size=batch_size,
        batch_norm=batch_norm,
        activation=activation,
        want_cuda=want_cuda,
        seed=seed,
        hidden_layers=hidden_layer
    )


def _train_mlpreg(argv):
    ndiff, batch_size, batch_norm, activation, want_cuda, seed, hidden_layer = argv
    # print(argv)
    train_degrad_mlp(
        ndiffs=ndiff,
        batch_size=batch_size,
        batch_norm=batch_norm,
        activation=activation,
        want_cuda=want_cuda,
        seed=seed,
        hidden_layers=hidden_layer
    )


def _train_anfis(argv):
    ndiff, batch_size, rules, want_cuda, seed = argv
    train_degrad_anfis(
        ndiffs=ndiff,
        batch_size=batch_size,
        rules=rules,
        want_cuda=want_cuda,
        seed=seed
    )


if __name__=="__main__":
    # Application CLI
    # ---------------
    from argparse import ArgumentParser

    parser = ArgumentParser(description="")  # TODO: description
    parser.add_argument(
        "mode",
        choices=("linreg", "fccreg", "anfis", "mlpreg"),
        default="linreg",
        help=""  # TODO: help
    )
    args = parser.parse_args()

    # General parameters
    # ------------------
    seeds = (42,)
    cvs = (7,)
    transforms = (False,)
    ndiffs = (0, 2, 4, 6, 8, 10, 12)

    # MLPRegressor parameters
    # -----------------------
    hidden_layer_sizes = (
        (3,), (2, 1),
        (6,), (4, 2), (4, 2), (2, 3, 1), (3, 2, 1),
        (9,), (6, 3), (3, 6), (7, 2), (2, 7), (3, 4, 2), (2, 4, 3)
    )
    activations = ("relu", "lrelu")

    # FCCRegressor parameters
    # -----------------------
    hidden_layers = (1, 3, 5, 7, 9, 11)
    batch_sizes = (1,)
    batch_norms = (False, True)
    want_cudas = (False,)

    # ANFIS parameters
    # ----------------
    rules = (2, 4, 6, 8)
    
    with mp.Pool(processes=int(mp.cpu_count() * 0.75)) as pool:
        if args.mode == "linreg":
            pool.map(
                _train_linreg,
                product(
                    seeds,
                    cvs,
                    transforms,
                    (*ndiffs, 100, 150, 200)
                )
            )
        #     )
        elif args.mode == "fccreg":
            pool.map(
                _train_fccreg,
                product(
                    ndiffs,
                    batch_sizes,
                    batch_norms,
                    activations,
                    want_cudas,
                    seeds,
                    hidden_layers
                )
            )
        elif args.mode == "mlpreg":
            pool.map(
                _train_mlpreg,
                product(
                    ndiffs,
                    batch_sizes,
                    batch_norms,
                    activations,
                    want_cudas,
                    seeds,
                    hidden_layer_sizes
                )
            )
        elif args.mode == "anfis":
            pool.map(
                _train_anfis,
                product(
                    ndiffs,
                    (*batch_sizes, 32),
                    rules,
                    want_cudas,
                    seeds
                )
            )
    
    print("DONE")
