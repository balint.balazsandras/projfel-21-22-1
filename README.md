# Projektfeladat 2021-22-1

A documentation of the code and API is hosted via [Read the docs](https://projektfeladat-2021-22-1.readthedocs.io/en/latest/index.html).

## Setting things up

The commands themselves provided here are for Ubuntu/Linux, however, the steps should be the same for under Windows.

- Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) (mainly for Windows).

- Install [Anaconda](https://www.anaconda.com/).

- Clone the repository.

  `git clone git@gitlab.com:balint.balazsandras/projfel-21-22-1.git`

  `git clone https://gitlab.com/balint.balazsandras/projfel-21-22-1.git`

1. Install environment.

   `conda env create -f path/to/projfel-21-22-1/env.yml`

2. Activate environment

   `conda activate projfel`

3. Optionally, set up the git hooks for version controlling jupyter notebooks with jupytext. The script itself should run on Windows, too; however, linking it to the `.git/hooks` folder may require more effort than it is worth.

   `cd path/to/projfel-21-22-1/.git/hooks`

   `ln -s ../../hooks/post_merge.py post-merge`

   Should one need to make the script executable, run

   `chmod +x path/to/projfel-21-22-1/hooks/post_merge.py`

## Importing experiments

The main task in the project was to actually fit models onto degradation data. The relevant experiments come as [MLflow](https://mlflow.org/docs/latest/tracking.html#where-runs-are-recorded) logs, i.e., as the zipped `mlruns` directory. To extract these / make these visible via the [web UI](https://mlflow.org/docs/latest/tracking.html#tracking-ui), run the following from a terminal (the `-q` flag suppresses the command `unzip` writing run information to the terminal)

  `cd path/to/projfel-21-22-1/`

  `unzip -q mlruns.zip`

The logs should then be accessible by running and visiting the web UI of MLflow (default `localhost:5000`) via a browser. See also section Accessing results via MLflow.

## Running experiments

1. Activate environment.

   `conda activate projfel`

2. Navigate to project folder.

   `cd path/to/projfel-21-22-1/`

- Optionally, get help in console/terminal on script parameters.

  `python train_parityN_fcc.py --help`

3. For example, train FCC NN for parity-9 with default settings and with max. batch size.

   `python train_parityN_fcc.py 9`

   `python train_parityN_fcc.py 9 --batch_size 512`

## Accessing results via MLflow

1. Activate environment.

   `conda activate projfel`

2. From the folder that contains the folder `mlruns`, run

   `mlflow ui`

3. Visit the address below via a browser.

   `localhost:5000`
