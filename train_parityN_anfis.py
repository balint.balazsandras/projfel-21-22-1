"""Train an adaptive neuro-fuzzy inference system (ANFIS) to solve a parity-N problem.
"""
import sys
from pathlib import Path
import time
import random
import torch
from torch import nn
from torch.utils.data import DataLoader
import mlflow
from urllib.parse import urlparse

from model.anfis import ANFIS
from data.ParityN.dataset import ParityN
from util.early_stopping.pytorchtools import EarlyStopping
from util.pytorch_training_tools import train_loop
from util.pytorch_training_tools import test_loop

DEFAULT_DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
"""Pytorch device string that is 'cuda' if available."""

BATCH_SIZE = 1
"""Default number of samples in a batch that the model 'sees' before updating its weights."""

EPOCHS = sys.maxsize * 2 + 1
"""Default number or epochs to train for.

The value is the interpreter's maximal word length, i.e., the biggest value that can be stored in an unsigned variable.
That is, infinity in practice, as the training should be stopped via early stopping.
"""

RULES = 2
"""Default number of rules in the rulebase."""

SEED = 42
"""Default seed to use in the experiment"""

DELTA = 1e-5
"""Default loss-change threshold between epochs for early stopping training."""


def train_parityN_anfis(
    bitnumber: int,
    batch_size: int = 1,
    epochs: int = EPOCHS,
    rules: int = RULES,
    want_cuda: bool = False,
    delta: float = DELTA,
    seed: int = SEED
):
    """Train an ANFIS to solve a parity-N problem.

    Parameters
    ----------
    bitnumber : int
        The N in the parity-N
    batch_size : int, optional
        Number of samples to show the model before updating its weights, by default 1
    epochs : int, optional
        Number of full iterations over the training set to train for, by default `EPOCHS`
    rules : int, optional
        Number of rules to use in the rulebase, by default `RULES`
    want_cuda : bool, optional
        Whether using the GPU is preferred, by default `False`
    delta : float, optional
        Threshold for loss-drop over 10 epochs to early-stop the training, by default `DELTA`
    seed : int, optional
        Experiment random seed, by default `SEED`
    """
    _ = mlflow.set_experiment(experiment_name="Parity-N")

    device = DEFAULT_DEVICE if want_cuda else "cpu"
    torch.random.manual_seed(seed)
    random.seed(seed)  # seeding Python, just in case

    with mlflow.start_run():
        # Get Dataset
        # -----------
        parityN = ParityN(N=bitnumber)

        # Init Dataloader
        # ---------------
        dataloader = DataLoader(
            parityN,
            batch_size=batch_size,
            pin_memory=want_cuda
        )

        # Init model
        # ---------------
        model = ANFIS(
            in_features=bitnumber,
            rules=rules,
            out_features=1,
            device=device
        )
        print("--- RULEBASE BEFORE ---")
        model.print_rulebase()
        print("--- TRAINING ---")

        # Init loss function
        # ------------------
        loss_fn = nn.MSELoss(reduction="sum")

        # Init optimizer
        # --------------
        optimizer = torch.optim.Adamax(model.parameters())

        # Init EarlyStopping
        # ------------------
        # to track losses as the model trains
        avg_train_losses = []
        avg_valid_losses = []
        # initialize the early_stopping object
        best_model = Path(__file__).resolve().parent / "logs" / "checkpoints" / f"checkpoint_{mlflow.active_run().info.run_id}.pt"
        best_model.parent.mkdir(exist_ok=True, parents=True)
        early_stopping = EarlyStopping(
            patience=10,
            verbose=True,
            path=best_model,
            delta=delta
        )

        for t in range(epochs):
            print(f"Epoch {t+1:3d}", end=" | ")

            # train
            # -----
            avg_train_loss = train_loop(
                dataloader,
                model,
                loss_fn,
                optimizer,
                device
            )
            avg_train_losses.append(avg_train_loss)

            # validate
            # --------
            valid_loss, accuracy = test_loop(
                dataloader,
                model,
                loss_fn,
                device
            )
            avg_valid_losses.append(valid_loss)
            
            mlflow.log_metric(
                key="loss",
                value=valid_loss,
                step=t
            )
            mlflow.log_metric(
                key="accuracy",
                value=accuracy,
                step=t
            )
            mlflow.log_metric(
                key="elapsed_time",
                value=time.thread_time(),
                step=t
            )

            # early stop
            # ----------
            early_stopping(
                valid_loss,
                model
            )
            if early_stopping.early_stop:
                print("Early stopping")
                break
        
        mlflow.log_param("N", bitnumber)
        mlflow.log_param("model", "anfis")
        mlflow.log_param("rules", rules)
        mlflow.log_param("batch_size", batch_size)
        mlflow.log_param("epochs", epochs)
        mlflow.log_param("delta", delta)
        mlflow.log_param("device", device)
        mlflow.log_param("seed", seed)

        mlflow.log_metric("early_stopped", int(early_stopping.early_stop))
        mlflow.log_metric("best_score", early_stopping.best_score)

        # load back best model
        model.load_state_dict(torch.load(best_model))
        model.eval()

        # log best model
        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme
        if tracking_url_type_store != "file":  # Model registry does not work with file store
            # Register the model
            mlflow.sklearn.log_model(model, "model", registered_model_name="ANFIS")
        else:
            mlflow.sklearn.log_model(model, "model")

        print("--- RULEBASE AFTER ---")
        model.print_rulebase()


if __name__=="__main__":
    # Application CLI
    # ---------------
    from argparse import ArgumentParser

    parser = ArgumentParser(
        description="Train an adaptive neuro-fuzzy inference system (ANFIS) to solve a parity-N problem."
    )
    parser.add_argument(
        "bitnumber",
        metavar="N",
        type=int,
        nargs=1,
        help="the N in the parity-N"
    )
    parser.add_argument(
        "--batch_size",
        type=int,
        default=BATCH_SIZE,
        help=f"number of samples shown to the model between weight updates; default {BATCH_SIZE}"
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=EPOCHS,
        help=f"number of epochs to train for; default {EPOCHS}"
    )
    parser.add_argument(
        "--rules",
        type=int,
        default=RULES,
        help=f"number of rules in the rulesbase; default {RULES}"
    )
    parser.add_argument(
        "--want_cuda",
        action="store_true",
        help="whether using the GPU is preferred"
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=SEED,
        help=f"experiment random seed; default {SEED}"
    )
    parser.add_argument(
        "--delta",
        type=float,
        default=DELTA,
        help=f"threshold for loss-drop to stop the training over; default {DELTA}"
    )

    args = parser.parse_args()
    args.bitnumber = args.bitnumber[0]  # a bit of neccessary post-processing

    train_parityN_anfis(**vars(args))
