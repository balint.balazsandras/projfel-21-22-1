"""Provides an implementation to an iteratively calculated average.

Examples
--------
>>> from util.rolling import rolling_avg
>>> myrange = range(5)
>>> average = sum(myrange) / len(myrange)
>>> print("average :", average)
average : 2.0
>>> average, n_sample = 0, 0
>>> for sample in myrange:
...   average, n_sample = rolling_avg(average, n_sample, sample)
>>> print("average :", average)
average : 2.0

API
---
"""


def rolling_avg(average, n_sample, new_sample) -> tuple:
    """Calculate average iteratively.

    Parameters
    ----------
    average
        Average value over the last `n_sample` values
    n_sample
        Number of samples thus far
    new_sample
        New sample to extend the average calculation over

    Returns
    -------
    tuple
        Tuple of the recalculated average and number of samples
    """
    average = n_sample / (n_sample + 1) * average + 1 / (n_sample + 1) * new_sample
    return average, n_sample + 1
