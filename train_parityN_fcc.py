"""Train a fully connected cascade (FCC) neural network (NN) topology to solve a parity-N problem.
"""
import sys
from pathlib import Path
import time
import random
import torch
from torch import nn
from torch.utils.data import DataLoader
import mlflow
from urllib.parse import urlparse

from model.fcc import FCCRegressor
from model.fcc import DEFAULT_DEVICE
from model.fcc import ACTIVATIONS
from data.ParityN.dataset import ParityN
from util.early_stopping.pytorchtools import EarlyStopping
from util.pytorch_training_tools import train_loop
from util.pytorch_training_tools import test_loop

EPOCHS = sys.maxsize * 2 + 1
"""Default number or epochs to train for.

The value is the interpreter's maximal word length, i.e., the biggest value that can be stored in an unsigned variable.
That is, infinity in practice, as the training should be stopped via early stopping.
"""

BATCH_SIZE = 1
"""Default number of samples in a batch that the model 'sees' before updating its weights."""

HIDDEN_LAYERS = 9
"""Default number of hidden layers.

The first experiments were run on the parity-9 problem, hence the value.
"""

ACTIVATION = "relu"
"""Default non-linear activation after the hidden layers.

It was apparently more prudent to go for one that is available both in scikit-learn and PyTorch.
Also, scaling to avoid saturation is not necessarily at the top of one's list when implementing things. 
"""

SEED = 42
"""Default random seed to use for the experiments."""

DELTA = 1e-5
"""Default loss-change threshold over 10 epochs for early stopping training."""


def train_parityN_fcc(
    bitnumber: int,
    batch_size: int = BATCH_SIZE,
    epochs: int = EPOCHS,
    hidden_layers: int = HIDDEN_LAYERS,
    activation: str = ACTIVATION,
    want_cuda: bool = False,
    delta: float = DELTA,
    seed: int = SEED
) -> None:
    """Train an FCC network to solve a parity-N problem.

    Parameters
    ----------
    bitnumber : int
        The N in the parity-N
    batch_size : int, optional
        Number of samples shown to the model between weight updates, by default `BATCH_SIZE`
    epochs : int, optional
        Number of full iterations over the training set to train for, by default `EPOCHS`
    hidden_layers : int, optional
        Number of FCC hidden layers, i.e., number of neurons-1, by default `HIDDEN_LAYERS`
    activation : str, optional
        Non-linear activation between hidden layers, see `~model.fcc.ACTIVATIONS`, by default `ACTIVATION`
    want_cuda : bool, optional
        Whether using the GPU is preferred, by default `False`
    delta : float, optional
        Threshold for loss-drop over 10 epochs to early-stop the training, by default `DELTA`
    seed : int, optional
        Experiment random seed, by default `SEED`
    """
    _ = mlflow.set_experiment(experiment_name="Parity-N")

    device = DEFAULT_DEVICE if want_cuda else "cpu"
    torch.manual_seed(seed)  # seeding PyTorch
    random.seed(seed)  # seeding Python, just in case

    with mlflow.start_run():
        # Get Dataset
        # -----------
        parity9 = ParityN(N=bitnumber)

        # Init Dataloader
        # ---------------
        dataloader = DataLoader(
            parity9,
            batch_size=batch_size,
            pin_memory=want_cuda
        )

        # Init model
        # ----------
        model = FCCRegressor(
            in_features=bitnumber,
            hidden_layers=hidden_layers,
            out_features=1,
            bias=True,
            activation=activation,
            device=device
        )

        # Init loss function
        # ------------------
        loss_fn = nn.MSELoss(reduction="sum")

        # Init optimizer
        # --------------
        optimizer = torch.optim.Adamax(model.parameters())

        # Init EarlyStopping
        # ------------------
        # to track losses as the model trains
        avg_train_losses = []
        avg_valid_losses = []
        # initialize the early_stopping object
        best_model = Path(__file__).resolve().parent / "logs" / "checkpoints" / f"checkpoint_{mlflow.active_run().info.run_id}.pt"
        best_model.parent.mkdir(exist_ok=True, parents=True)
        early_stopping = EarlyStopping(
            patience=10,
            verbose=True,
            path=best_model,
            delta=delta
        )

        for t in range(epochs):
            print(f"Epoch {t+1:3d}", end=" | ")
            
            # train
            # -----
            avg_train_loss = train_loop(
                dataloader,
                model,
                loss_fn,
                optimizer,
                device=device
            )
            avg_train_losses.append(avg_train_loss)

            # validate
            # --------
            valid_loss, accuracy = test_loop(
                dataloader,
                model,
                loss_fn,
                device=device
            )
            avg_valid_losses.append(valid_loss)

            mlflow.log_metric(
                key="loss",
                value=valid_loss,
                step=t
            )
            mlflow.log_metric(
                key="accuracy",
                value=accuracy,
                step=t
            )
            mlflow.log_metric(
                key="elapsed_time",
                value=time.thread_time(),
                step=t
            )

            # early stop
            # ----------
            early_stopping(
                valid_loss,
                model
            )
            if early_stopping.early_stop:
                print("Early stopping")
                break
        
        mlflow.log_param("N", bitnumber)
        mlflow.log_param("model", "fcc")
        mlflow.log_param("hidden_layers", hidden_layers)
        mlflow.log_param("activation", activation)
        mlflow.log_param("batch_size", batch_size)
        mlflow.log_param("epochs", epochs)
        mlflow.log_param("delta", delta)
        mlflow.log_param("device", device)
        mlflow.log_param("seed", seed)
        
        mlflow.log_metric("early_stopped", int(early_stopping.early_stop))
        mlflow.log_metric("best_score", early_stopping.best_score)

        # load back best model
        model.load_state_dict(torch.load(best_model))
        model.eval()

        # log best model
        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme
        if tracking_url_type_store != "file":  # Model registry does not work with file store
            # Register the model
            mlflow.sklearn.log_model(model, "model", registered_model_name="FCCRegressor")
        else:
            mlflow.sklearn.log_model(model, "model")


if __name__=="__main__":
    # Application CLI
    # ---------------
    from argparse import ArgumentParser

    parser = ArgumentParser(
        description="Train a fully connected cascade (FCC) neural network (NN) to solve a parity-N problem."
    )
    parser.add_argument(
        "bitnumber",
        metavar="N",
        type=int,
        nargs=1,
        help="the N in the parity-N"
    )
    parser.add_argument(
        "--batch_size",
        type=int,
        default=BATCH_SIZE,
        help=f"number of samples shown to the model between weight updates; default {BATCH_SIZE}"
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=EPOCHS,
        help=f"number of epochs to train for; default {EPOCHS}"
    )
    parser.add_argument(
        "--hidden_layers",
        type=int,
        default=HIDDEN_LAYERS,
        help=f"number of model hidden layers, i.e., number of neurons - 1; default {HIDDEN_LAYERS}"
    )
    parser.add_argument(
        "--activation",
        type=str,
        choices=ACTIVATIONS,
        default=ACTIVATION,
        help=f"non-linear activation between hidden layers; default {ACTIVATION}"
    )
    parser.add_argument(
        "--want_cuda",
        action="store_true",
        help="whether using the GPU is preferred"
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=SEED,
        help=f"experiment random seed; default {SEED}"
    )
    parser.add_argument(
        "--delta",
        type=float,
        default=DELTA,
        help=f"threshold for loss-drop to stop the training over; default {DELTA}"
    )

    args = parser.parse_args()
    args.bitnumber = args.bitnumber[0]  # a bit of necessary post-processing

    train_parityN_fcc(**vars(args))
