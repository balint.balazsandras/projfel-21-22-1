"""Provide the implementation of the `torch.utils.data.Dataset` API."""
import torch
from torch.utils.data import Dataset
from typing import Union
from pathlib import Path
from functools import lru_cache

from .preprocess import DEFAULT_DATA
from .preprocess import load_path_df
from .preprocess import separate_paths
from .preprocess import calculate_diffs
from .preprocess import rebase_paths
from .preprocess import merge_paths


class DegradationPathDataset(Dataset):
    """A map-type `~torch.utils.data.Dataset` for predicting predicted time to failure (PTTF) based on a degradation metric.
    
    Parameters
    ----------
    data_file : ~typing.Union[str, ~pathlib.Path], optional
        Pathlike that points to the CSV containing the data, by default `None`, i.e., `~data.Degradation.preprocess.DEFAULT_DATA`
    ndiffs : int, optional
        Number of previous degradation samples to extend the records with, by default 2

    Attributes
    ----------
    data : ~pandas.DataFrame
        Data in the dataset, i.e., the degradation samples
    label : ~pandas.DataFrame
        Labels corresponding to the data, i.e., the PTTF values
    """

    def __init__(self,
                 data_file: Union[str, Path]=None,
                 ndiffs: int = 2):
        path_df = load_path_df(DEFAULT_DATA if data_file is None else data_file)
        paths = separate_paths(path_df)
        paths = calculate_diffs(paths, n=ndiffs)
        paths = rebase_paths(paths)
        self.path_df = merge_paths(paths)

        self.data = self.path_df.drop("PTTF", axis=1)
        self.label = self.path_df["PTTF"].copy()

    def __len__(self):
        return self.data.shape[0]

    @lru_cache
    def __getitem__(self, idx):
        return torch.Tensor(self.data.iloc[idx].values.tolist()), torch.Tensor([self.label.iloc[idx]])
