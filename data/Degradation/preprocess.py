"""Functions wrapping degradation data preprocessing."""
from pathlib import Path
import pandas as pd
import numpy as np
from sklearn import preprocessing as prep
from typing import Union
from typing import List
from contextlib import ContextDecorator
from sklearn.model_selection import train_test_split


THIS_FILE = str(Path(__file__).resolve())
"""`~pathlib.Path` to the module .py file.

Notes
-----
The exact value shown is irrelevant as it is determined according to the local setup.
"""

DEFAULT_DATA = str(Path(THIS_FILE).parent / "Degradation path_01.csv")
"""Default `~pathlib.Path` to the data file that is to be loaded.

Notes
-----
The exact value shown is irrelevant as it is determined according to the local setup by default as `THIS_FILE`/../data/Degradation path_01.csv.

It is assumed that the data file follows the CSV format and that it has a single column with a header "degradation".
"""


class ignoreSettingWithCopyWarning(ContextDecorator):
    """A `~contextlib.ContextDecorator` to ignore the pandas `SettingWithCopyWarning`."""

    def __enter__(self):
        self.chained_assignment = pd.options.mode.chained_assignment
        pd.options.mode.chained_assignment = None
        return self

    def __exit__(self, *exc):
        pd.options.mode.chained_assignment = self.chained_assignment
        return False


def load_path_df(data_file: Union[str, Path]) -> pd.DataFrame:
    """Loads data as a `~pandas.DataFrame`.

    Parameters
    ----------
    data_file : ~typing.Union[str, ~pathlib.Path]
        Path to the data file, see `~pandas.read_csv`

    Returns
    -------
    path_df : ~pandas.DataFrame
        Loaded data.
    """
    return pd.read_csv(data_file)


def separate_paths(path_df: pd.DataFrame) -> List[pd.DataFrame]:
    """Separates the degradation data into chunks between maintenances.

    Parameters
    ----------
    path_df : ~pandas.DataFrame
        Degradation data, see `load_path_df`

    Returns
    -------
    paths : ~typing.List[~pandas.DataFrame]
        List of degradation data chunks.
    """
    maintenances = [0] + path_df.index[path_df["degradation"].diff() < 0].tolist()
    # sourcery skip: inline-immediately-returned-variable
    paths = [path_df.iloc[maintenances[i]:maintenances[i+1]] for i in range(len(maintenances) - 1)]
    return paths


@ignoreSettingWithCopyWarning()
def calculate_diffs(paths: List[pd.DataFrame], n: int = 2) -> List[pd.DataFrame]:
    """Adds columns of degradation data shifted forward over sampling steps.
    
    Parameters
    ----------
    paths : ~pandas.DataFrame
        List of degradation data chunks
    n : int, optional
        The number of shifts to do. E.g. n=2 means 2 new columns: one shifted once, the other shifted twice

    Returns
    -------
    paths : ~typing.List[~pandas.DataFrame]
        List of degradation data chunks with the previous values

    Notes
    -----
    Applying this function results in the rows of data containing the last n+1 samples of degradation.
    """
    if n <= 0:
        return paths
    for path in paths:
        for i in range(n):
            path["degradation.t-{}".format(i+1)] = path["degradation"].shift(periods=i+1, fill_value=0)
    return paths


@ignoreSettingWithCopyWarning()
def rebase_paths(paths: List[pd.DataFrame]) -> List[pd.DataFrame]:
    """Calculates the time to failure for each record and adds it as a new column.
    
    Parameters
    ----------
    paths : ~typing.List[~pandas.DataFrame]
        List of degradation data chunks

    Returns
    -------
    paths : ~typing.List[~pandas.DataFrame]
        List of degradation data chunks with the times to failure
    
    Notes
    -----
    After calculating the differences, this function also removes the very last records in every chunk of degradation data as those are measurements from right after the failure happened.  
    """
    for path in paths:
        maint = path.index.to_numpy()
        maint = maint - maint[0]
        maint = maint - maint[-1]
        path["PTTF"] = maint
    paths = [path[:-1] for path in paths]
    return paths


def merge_paths(paths: List[pd.DataFrame]) -> pd.DataFrame:
    """Re-merges the chunks of degradation data.
    
    Parameters
    ----------
    paths : ~typing.List[~pandas.DataFrame]
        List of degradation data chunks

    Returns
    -------
    path_df : ~pandas.DataFrame
        Re-merged paths
    """
    path_df = None
    for path in paths:
        path_df = path if path_df is None else path_df.append(path, ignore_index=True)
    return path_df


def preprocess(data_file: Union[str, Path]=DEFAULT_DATA, ndiffs: int = 2, test_size: float = 0.2, seed: int = 42, transform: bool = False):
    """Wraps a complete data preprocession pipeline.

    Parameters
    ----------
    data_file : ~typing.Union[str, ~pathlib.Path], optional
        Pathlike `str` or `~pathlib.Path` to the data file, by default `DEFAULT_DATA`
    ndiffs : int, optional
        Number of previous degradation samples to add to a record, by default 2, see `calculate_diffs`
    test_size : float, optional
        Fraction of the test set wrt. to the whole dataset, by default 0.2
    seed : int, optional
        Seed to use in the random splitting of the dataset, by default 42
    transform : bool, optional
        Whether to fit and apply a `~sklearn.preprocessing.RobustScaler` to the train set, by default `False`

    Returns
    -------
    ~typing.Tuple[~pandas.DataFrame, ~pandas.DataFrame, ~sklearn.preprocessing.RobustScaler]
        Tuple of (train set, test set, robust scaler)
    
    Notes
    -----
    The `~sklearn.preprocessing.RobustScaler` feature is there, but not really supported currently.
    """
    path_df = load_path_df(DEFAULT_DATA if data_file is None else data_file)
    paths = separate_paths(path_df)
    paths = calculate_diffs(paths, n=ndiffs)
    paths = rebase_paths(paths)
    path_df = merge_paths(paths)
    
    train_set, test_set = train_test_split(path_df, test_size=test_size, random_state=seed)

    transformer = prep.RobustScaler().fit(train_set)
    apply_trf = lambda df: pd.DataFrame(transformer.transform(df), columns=list(df.columns))

    return apply_trf(train_set) if transform else train_set, apply_trf(test_set) if transform else test_set, transformer


def inverse_transform_scores(scorelist, column, base_df, transformer):
    # sourcery skip: inline-immediately-returned-variable
    invd_scores = transformer.inverse_transform(scorelist)
    return invd_scores


if __name__=="__main__":
    # CLI
    from argparse import ArgumentParser

    parser = ArgumentParser(description="")  # TODO: description
    parser.add_argument("--data_file",
                        type=str,
                        default=DEFAULT_DATA,
                        help=f"default: {DEFAULT_DATA}") # TODO: add help
    parser.add_argument("--ndiffs",
                        type=int,
                        default=0,
                        help="default: 0") # TODO: add help
    args = parser.parse_args()

    # LOAD DATA
    train_set, test_set, transformer = preprocess(**vars(args), transform=True)

    test_neg_mean_absolute_error = np.array([-0.35587969, -0.36964796, -0.35960101, -0.35866874, -0.3559948, -0.37952256, -0.36352933])
    test_max_error = np.array([-1.62332982, -1.61156508, -1.60036696, -1.58558892, -1.61750584, -1.62185048, -1.5933858])

    magix = lambda x: np.repeat(np.expand_dims(x, axis=0), 2, axis=0).T

    # scores = inverse_transform_scores(
    #     test_neg_mean_absolute_error, "PTTF", train_set, transformer)

    print(transformer.inverse_transform(magix(test_neg_mean_absolute_error * -1)))
    print(transformer.inverse_transform(magix(test_max_error)))
