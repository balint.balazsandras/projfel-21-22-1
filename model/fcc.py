"""Provides a PyTorch implementation for the fully connected cascaded (FCC) neural net (NN) topology both for regression and classification.

Examples
--------
>>> import torch
>>> from model.fcc import FCCRegressor
>>> from model.fcc import DEFAULT_DEVICE
>>> want_cuda = True
>>> device = DEFAULT_DEVICE if want_cuda else None
>>> net = FCCRegressor(in_features=2, hidden_layers=2, out_features=2, device=device)
>>> x = torch.randn(2, device=device)
>>> batch = torch.stack((x, x), 0)
>>> print(net)
FCCRegressor(
  (layer_stack): Sequential(
    (0): FCCLayer(
      (linear): Linear(in_features=2, out_features=1, bias=True)
      (activation): ReLU()
    )
    (1): FCCLayer(
      (linear): Linear(in_features=3, out_features=1, bias=True)
      (activation): ReLU()
    )
    (2): Linear(in_features=4, out_features=2, bias=True)
  )
)
>>> print(net(x))
tensor([0.2840, 0.0225], grad_fn=<AddBackward0>)
>>> print(net(batch))
tensor([[0.2840, 0.0225],
        [0.2840, 0.0225]], grad_fn=<AddmmBackward0>)

API
---
"""
import torch
from torch import nn
from torch import Tensor

DEFAULT_DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
"""Default device on which to set `~torch.Tensors` up. 'cuda' if it is available, 'cpu' otherwise."""

ACTIVATIONS = {
    "relu": nn.ReLU,
    "identity": nn.Identity,
    "logistic": nn.Sigmoid,
    "tanh": nn.Tanh,
    "softmax": lambda *args, **kwargs: nn.Softmax(dim=-1, *args, **kwargs),
    "lrelu": nn.LeakyReLU
}
"""Dictionary of useable non-linear activation."""

INITIALIZATIONS = {
    "identity": nn.init.xavier_normal_,
    "tanh": nn.init.xavier_normal_,
    "logistic": nn.init.xavier_normal_,
    "relu": lambda x: nn.init.kaiming_normal_(x, a=0, mode="fan_in", nonlinearity="relu"),
    "lrelu": lambda x: nn.init.kaiming_normal_(x, a=0, mode="fan_in", nonlinearity="relu")
}
"""Dictionary that maps useable activation functions to default `~torch.Tensor` initialization methods.

The rationale and considerations behind this mapping are based on this book [1]_.

References
----------
.. [1] GeÌron, Aureìlien. Hands-on Machine Learning with Scikit-Learn, Keras and TensorFlow: Concepts, Tools, and Techniques to Build Intelligent Systems. 2nd ed., O’Reilly, 2019.
"""


class FCCLayer(nn.Module):
    """Implements a single hidden layer for a fully connected cascaded (FCC) neural network (NN).
    
    Parameters
    ----------
    in_features : int
        Number of (flattened) input features
    activation : str, optional
        Non-linear activation to apply, by default "relu", see `ACTIVATIONS`
    batch_normalization : bool, optional
        Whether to apply batch normalization before the non-linear activation, by default `False`
    bias : bool, optional
        Whether to use bias terms in the linear layers, by default `True`
    eps : float, optional
        See `~torch.nn.BatchNorm1d`, by default 1e-05
    momentum : float, optional
        See `~torch.nn.BatchNorm1d`, by default 0.1
    affine : bool, optional
        See `~torch.nn.BatchNorm1d`, by default `True`
    track_running_stats : bool, optional
        See `~torch.nn.BatchNorm1d`, by default `True`
    device : optional
        Device on which to set `~torch.Tensors` up, by default `None`, see `~torch.nn.Linear`
    dtype : optional
        Datatype of parameters, by default `None`, see `~torch.nn.Linear`

    Attributes
    ----------
    in_features : int
        Number of (flattened) input features
    out_features : int
        Number of output features
    bias : bool, optional
        Whether bias terms are used in the linear layer, by default `True`
    linear : ~torch.nn.Linear
        The (single) neuron of the layer
    activation : ~torch.nn.Module
        Non-linear activation applied after the neuron, see `ACTIVATIONS`
    batch_normalization : ~torch.nn.Module
        Batch normalization layer, if applied after the non-linear activation

    Raises
    ------
    AssertionError
        if the passed activation is not a key in `ACTIVATIONS`
    """
    
    def __init__(self,
                 in_features: int,
                 activation: str = "relu",
                 batch_normalization: bool = False,
                 bias: bool = True,
                 eps: float = 1e-05,
                 momentum: float = 0.1,
                 affine: bool = True,
                 track_running_stats: bool = True,
                 device=None,
                 dtype=None) -> None:
        factory_kwargs = {"device": device, "dtype": dtype}
        super(FCCLayer, self).__init__()

        self.in_features = in_features
        self.out_features = self.in_features + 1
        self.bias = bias
        self.linear = nn.Linear(in_features=in_features,
                                out_features=1,
                                bias=bias,
                                **factory_kwargs)

        assert activation in ACTIVATIONS, f"Activation '{activation}' is not implemented."
        self.activation = ACTIVATIONS[activation]()
        INITIALIZATIONS[activation](self.linear.weight)
        # bias and INITIALIZATIONS[activation](self.linear.bias)

        self.batch_normalization = nn.BatchNorm1d(
            num_features=1,
            eps=eps,
            momentum=momentum,
            affine=affine, track_running_stats=track_running_stats,  # TODO: implement a more educated training?
            **factory_kwargs) if batch_normalization else None

    def forward(self, x: Tensor) -> Tensor:
        # sourcery skip: inline-immediately-returned-variable
        linear = self.linear(x)
        non_linear = self.activation(linear)
        if self.batch_normalization is not None:
            non_linear = self.batch_normalization(non_linear)
        output = torch.cat((x, non_linear), x.dim() - 1)
        return output


class FCCRegressor(nn.Module):
    """Implements a neural net regressor with the fully connected cascaded (FCC) topology.
    
    Parameters
    ----------
    in_features : int, optional
        Number of (flattened) input features, by default 1
    hidden_layers : int, optional
        Number of FCC layers, by default 1
    out_features : int, optional
        Number of output features, by default 1
    bias : bool, optional
        Whether to use bias terms in the linear layers, by default `True`
    activation : str, optional
        Non-linear activation to apply for the hidden layers, by default "relu", see `ACTIVATIONS`
    output_activation : str, optional
        Non-linear activation to apply for the output layer, by default `None`, see `ACTIVATIONS`
    batch_normalization : bool, optional
        Whether to apply batch normalization before the non-linear activation, by default `False`
    eps : float, optional
        See `~torch.nn.BatchNorm1d`, by default 1e-05
    momentum : float, optional
        See `~torch.nn.BatchNorm1d`, by default 0.1
    affine : bool, optional
        See `~torch.nn.BatchNorm1d`, by default `True`
    track_running_stats : bool, optional
        See `~torch.nn.BatchNorm1d`, by default `True`
    device : optional
        Device on which to set `~torch.Tensor` up, by default `None`, see e.g. `~torch.nn.Linear`
    dtype : optional
        Datatype of parameters, by default `None`, see e.g. `~torch.nn.Linear`

    Attributes
    ----------
    layer_stack : ~torch.nn.Sequential
        PyTorch layer stack, i.e., the actual model
    """
    # TODO: remark on batch normalization?
    # TODO: remark on output activation?

    def __init__(self,
                 in_features: int = 1,
                 hidden_layers: int = 1,
                 out_features: int = 1,
                 bias: bool = True,
                 activation: str = "relu",
                 output_activation: str = None,
                 batch_normalization: bool = False,
                 eps: float = 1e-05,
                 momentum: float = 0.1,
                 affine: bool = True,
                 track_running_stats: bool = True,
                 device=None,
                 dtype=None) -> None:
        factory_kwargs = {"device": device, "dtype": dtype}
        super(FCCRegressor, self).__init__()

        layer_stack = [nn.BatchNorm1d(num_features=in_features,
                                      eps=eps,
                                      momentum=momentum,
                                      affine=affine,
                                      track_running_stats=track_running_stats,
                                      **factory_kwargs)] if batch_normalization else []
        # sourcery skip: aug-assign
        layer_stack = layer_stack + [FCCLayer(in_features=in_fts,
                                              activation=activation,
                                              batch_normalization=batch_normalization,
                                              bias=bias,
                                              eps=eps,
                                              momentum=momentum,
                                              affine=affine,
                                              track_running_stats=track_running_stats,
                                              **factory_kwargs) for in_fts in range(in_features, in_features + hidden_layers)]

        layer_stack += [nn.Linear(in_features=(in_features + hidden_layers),
                                  out_features=out_features,
                                  bias=bias,
                                  **factory_kwargs)]

        assert output_activation is None or output_activation in ACTIVATIONS, f"Output activation '{output_activation}' is not implemented."
        if output_activation is not None:
            layer_stack += [ACTIVATIONS[output_activation]()]

        self.layer_stack = nn.Sequential(*layer_stack)

    def forward(self, x):  # sourcery skip: inline-immediately-returned-variable
        output = self.layer_stack(x)
        return output


class FCCClassifier(FCCRegressor):
    """Implements a neural net classifier with the fully connected cascaded (FCC) topology.

    Parameters
    ----------
    out_features : int, optional
        Number of output features, by default 2
    output_activation : str, optional
        Non-linear activation to apply for the output layer, by default "softmax", see `ACTIVATIONS`
    kwargs : dict, optional
        See `FCCRegressor`
    
    Attributes
    ----------
    layer_stack : ~torch.nn.Sequential
        PyTorch layer stack, i.e., the actual model
    """
    # TODO: remark on output activation?

    def __init__(self,
                 out_features: int = 2,
                 output_activation: str = "softmax",
                 **kwargs) -> None:
        super(FCCClassifier, self).__init__(out_features=out_features,
                                            output_activation=output_activation,
                                            **kwargs)


if __name__=="__main__":
    in_features = 2
    layer_no = 3
    want_cuda = True

    device = DEFAULT_DEVICE if want_cuda else None
    fcclayers = [FCCLayer(featureno, device=device) for featureno in range(in_features, in_features + layer_no)]
    networks = [nn.Sequential(*fcclayers[:layers]) for layers in range(1, 1 + layer_no)]

    x = torch.randn(in_features, device=device)
    x = torch.stack((x, x), 0)
    print("x", ":", x)
    [print(network(x)) for network in networks]

    net = FCCRegressor(2, 1, 3, device=device)
    print(net)
    print(net(x))
    