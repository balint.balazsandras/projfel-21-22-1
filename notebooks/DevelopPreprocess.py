# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # DevelopPreprocess
#
# This notebook was used mainly to facilitate the development of the data preprocess pipeline for the degradation dataset.

from pathlib import Path
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn import preprocessing as prep

# Add the repository root so that imports of local modules are possible

import sys
from __init__ import THIS_MODULE
sys.path.insert(0, str(Path(THIS_MODULE).parent))

# Import local modules

from data.Degradation.preprocess import THIS_FILE
from data.Degradation.preprocess import DEFAULT_DATA
from data.Degradation.preprocess import load_path_df
from data.Degradation.preprocess import separate_paths
from data.Degradation.preprocess import calculate_diffs
from data.Degradation.preprocess import rebase_paths
from data.Degradation.preprocess import merge_paths
from data.Degradation.preprocess import preprocess

# Configure matplotlib plotting

# %matplotlib notebook

# # Load and preprocess

train_set, test_set, transformer = preprocess(ndiffs=3)

# Print docstring

print(preprocess.__doc__)


